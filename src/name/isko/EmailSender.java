package name.isko;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailSender {
    //TODO: specify correct sender credentials before use
    private static final String GMAIL_SENDER_USERNAME = "";
    private static final String GMAIL_SENDER_PASSWORD = "";
    private static final String RECIPIENT_NAME = "Roman";
    private static final String PAGE_URL = Main.PAGE_URL;
    //TODO: specify recepient email address
    private static final String TO = "";

    /**
     * @param message email message
     */
    public void sendMessage(Message message) throws MessagingException {
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO));
        Transport.send(message);
        System.out.println("Email message was sent");
    }

    public Message composeMessage(String subject) {
        try {
            Message message = new MimeMessage(createSession());
            message.setText(String.format("Hello %s!\n" +
                    "I would like to notify you that sector or place is available\n" +
                    "\n" +
                    "You could buy several tickets on %s\n" +
                    "Hurry up! ;)", RECIPIENT_NAME, PAGE_URL));
            message.setSubject(subject);
            return message;
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static Session createSession() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        return Session.getInstance(properties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(GMAIL_SENDER_USERNAME, GMAIL_SENDER_PASSWORD);
                    }
                }
        );
    }
}