package name.isko;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.Iterator;
import java.util.List;

public class PlaceChecker {
    private WebDriver driver;
    private String sectorXpath;

    public PlaceChecker(WebDriver driver, String sectorXpath) {
        this.driver = driver;
        this.sectorXpath = sectorXpath;
    }

    public void checkPlaces(List<String> places) throws MessagingException {
        driver.findElement(By.xpath(sectorXpath)).click();
        Iterator iterator = places.iterator();
        while (iterator.hasNext()) {
            String place = (String) iterator.next();
            boolean pageContainsPlace = placeIsClickable(driver, place);
            if (pageContainsPlace) {
                System.out.println(String.format("Place %s is available", place));
                EmailSender sender = new EmailSender();
                Message message = sender.composeMessage(String.format("Place %s is available", place));
                sender.sendMessage(message);
                iterator.remove();
            }
        }
    }

    private static boolean placeIsClickable(WebDriver driver, String xPath) {
        return driver.findElements(By.xpath(xPath)).size() > 0;
    }
}
