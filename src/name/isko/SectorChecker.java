package name.isko;

import org.openqa.selenium.WebDriver;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.Iterator;
import java.util.List;

public class SectorChecker {
    private WebDriver driver;
    private List<String> sectors;

    public SectorChecker(List<String> sectors, WebDriver driver) {
        this.driver = driver;
        this.sectors = sectors;
    }

    public void checkSectors() throws MessagingException {
        Iterator iterator = sectors.iterator();
        while (iterator.hasNext()) {
            String pageSource = getPageSource(driver);
            String sector = (String) iterator.next();
            boolean pageContainsSector = pageContainsSector(pageSource, "Сектор " + sector);
            if (pageContainsSector) {
                System.out.println(String.format("The sector %s is clickable", sector));
                EmailSender sender = new EmailSender();
                Message message = sender.composeMessage(String.format("The sector %s is clickable", sector));
                sender.sendMessage(message);
                iterator.remove();
            }
        }
    }

    private static String getPageSource(WebDriver driver) {
        return driver.getPageSource();
    }

    private static boolean pageContainsSector(String pageSource, String pageSourceForSector) {
        return pageSource.contains(pageSourceForSector);
    }
}
