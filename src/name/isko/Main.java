package name.isko;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static final String PAGE_URL = "http://kasa.in.ua/ua/events/view-okean-elzy-20-rokiv-lviv.html";
    public static final List<String> REQUIRED_SECTORS = new ArrayList<>(Arrays.asList("14А", "14В"));
    public static final List<String> REQUIRED_PLACES_IN_06A = new ArrayList<>(Arrays.asList(
            "/html/body/div/div[2]/div[4]/div/div[5]/div/div/div/div/div/div/div[2]/a",
            "/html/body/div/div[2]/div[4]/div/div[5]/div/div/div/div/div/div/div[3]/a",
            "/html/body/div/div[2]/div[4]/div/div[5]/div/div/div/div/div/div/div[4]/a",
            "/html/body/div/div[2]/div[4]/div/div[5]/div/div/div/div/div/div/div[5]/a"));
    public static final List<String> REQUIRED_PLACES_IN_13A = REQUIRED_PLACES_IN_06A;

    private static final String SECTOR_06A_XPATH = "/html/body/div/div[2]/div[4]/div/div[4]/div/map/area[4]";
    private static final String SECTOR_13A_XPATH = "/html/body/div/div[2]/div[4]/div/div[4]/div/map/area[14]";

    public static void main(String[] args) throws MessagingException {
        WebDriver driver = new FirefoxDriver();
        driver.get(PAGE_URL);
        SectorChecker sectorChecker = new SectorChecker(REQUIRED_SECTORS, driver);
        PlaceChecker placeCheckerSector06A = new PlaceChecker(driver, SECTOR_06A_XPATH);
        PlaceChecker placeCheckerSector13A = new PlaceChecker(driver, SECTOR_13A_XPATH);
        while (!REQUIRED_PLACES_IN_06A.isEmpty() && !REQUIRED_PLACES_IN_13A.isEmpty()) {
            System.out.println("-------------------------");
            System.out.println("checking sectors");
            sectorChecker.checkSectors();
            System.out.println("-------------------------");
            System.out.println("Checking places");
            placeCheckerSector06A.checkPlaces(REQUIRED_PLACES_IN_06A);
            placeCheckerSector13A.checkPlaces(REQUIRED_PLACES_IN_13A);
            System.out.println("-------------------------");
            System.out.println("Waiting");
            waitWithoutReason(driver, 600);
            System.out.println("-------------------------");
        }
    }

    /**
     * @param driver  WebDriver
     * @param seconds seconds to wait
     */
    private static void waitWithoutReason(WebDriver driver, int seconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver d) {
                    return false;
                }
            });
        } catch (TimeoutException ignored) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}